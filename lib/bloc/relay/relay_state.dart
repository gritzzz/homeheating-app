// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'relay_bloc.dart';

@immutable
sealed class RelayState {}

final class RelayInitial extends RelayState {}

class GetRelayState extends RelayState {
  final bool isRelayOn;
  GetRelayState({
    required this.isRelayOn,
  });
}
class ErrorState extends RelayState {
  final String errorMessage;

  ErrorState(this.errorMessage);
}