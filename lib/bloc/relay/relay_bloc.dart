import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:bloc/bloc.dart';
import 'package:home_heating/data.dart';
import 'package:meta/meta.dart';

part 'relay_event.dart';
part 'relay_state.dart';

class RelayBloc extends Bloc<RelayEvent, RelayState> {
  RelayBloc() : super(RelayInitial()) {
    on<GetRelayEvent>(_getRelay);
  }

  _getRelay(RelayEvent event, Emitter<RelayState> emit) async {
    const url = "$webApiUrl/relaystatus";
    try {
      final response = await http.Client()
          .get(Uri.parse(url))
          .timeout(const Duration(seconds: 5));

      final parsed = jsonDecode(response.body);

      final bool isRelayOn = parsed['relayStatus'];

      emit(GetRelayState(isRelayOn: isRelayOn));
    } catch (e) {
      // print('Ошибка при запросе данных: $e');
      emit(ErrorState('Ошибка при запросе данных: $e'));
    }
  }
}
