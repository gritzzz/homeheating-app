import 'package:bloc/bloc.dart';
import 'package:home_heating/data.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

part 'data_event.dart';
part 'data_state.dart';

class DataBloc extends Bloc<DataEvent, DataState> {
  DataBloc() : super(DataInitial()) {
    on<GetTempAndHumEvent>(_getTempAndHum);
    on<GetMinMaxEvent>(_getMinMax);
  }

  _getTempAndHum(GetTempAndHumEvent event, Emitter<DataState> emit) async {
    emit(LoadingTempAndHumState());
   
    const url = "$webApiUrl/gettemperature";
    try {
    final response = await http.Client().get(Uri.parse(url)).timeout(const Duration(seconds: 5));

    final parsed = jsonDecode(response.body);
    
   final int temp = parsed['temperature']; 
   final int hum = parsed['humidity'];
   

    emit(GetTempAndHumState(temp,hum));
     } catch (e) {
       // print('Ошибка при запросе данных: $e');
    emit(ErrorState('Ошибка при запросе данных: $e'));
  }
  }
   _getMinMax(GetMinMaxEvent event, Emitter<DataState> emit) async {
    
   
    const url = "$webApiUrl/getminmax";
    try {
    final response = await http.Client().get(Uri.parse(url)).timeout(const Duration(seconds: 5));

    final parsed = jsonDecode(response.body);
    
   final int minTemp = parsed['min']; 
   final int maxTemp = parsed['max'];
   

    emit(GetMinMaxState(minTemp,maxTemp));
     } catch (e) {
       // print('Ошибка при запросе данных: $e');
    emit(ErrorState('Ошибка при запросе данных: $e'));
  }
  }
}