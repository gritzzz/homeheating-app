// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:home_heating/bloc/data/data_bloc.dart';
import 'package:home_heating/bloc/relay/relay_bloc.dart';
import 'package:home_heating/bloc/send/send_bloc.dart';

class HeatingPage extends StatefulWidget {
  final DataBloc dataBloc;
  final RelayBloc relayBloc;
  final SendBloc sendBloc;
  const HeatingPage({
    Key? key,
    required this.dataBloc,
    required this.relayBloc,
    required this.sendBloc,
  }) : super(key: key);

  @override
  State<HeatingPage> createState() => _HeatingPageState();
}

class _HeatingPageState extends State<HeatingPage> {
  double _currentSliderPrimaryValue = 20;
  double _currentSliderSecondaryValue = 25;
  int _temperature = 0;
  int _humidity = 0;
  bool _relayStatus = true;
  bool _minMaxEventFired = false;

  late Timer _timer; // Добавьте переменную для таймера

  @override
  void initState() {
    super.initState();
    // Инициализируйте таймер при создании виджета
    _timer = Timer.periodic(const Duration(seconds: 5), (timer) {
      widget.dataBloc.add(GetTempAndHumEvent());
      widget.relayBloc.add(GetRelayEvent());
      if (!_minMaxEventFired) {
        widget.dataBloc.add(GetMinMaxEvent());
        _minMaxEventFired = true;
      }
    });
  }

  @override
  void dispose() {
    // Отмените таймер при удалении виджета
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider.value(value: widget.dataBloc),
          BlocProvider.value(value: widget.relayBloc),
          BlocProvider.value(value: widget.sendBloc),
        ],
        child: Scaffold(
            appBar: AppBar(title: const Text('Управление системой отопления')),
            body: BlocBuilder<DataBloc, DataState>(
                bloc: widget.dataBloc,
                builder: (context, state) {
                  // dataBloc.add(GetTempAndHumEvent());
                  if (state is GetTempAndHumState) {
                    _temperature = state.temperature;
                    _humidity = state.humidity;
                  }
                  if (state is GetMinMaxState) {
                    _currentSliderSecondaryValue = state.maxTemp.toDouble();
                    _currentSliderPrimaryValue = state.minTemp.toDouble();
                  }
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        /* decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.red, // Цвет границы
                          width: 2.0, // Ширина границы
                        ),
                      ),*/
                        width: double.infinity,
                        //height: MediaQuery.of(context).size.height * 0.3,
                        child: Column(
                          children: [
                            const Text('Температура',
                                style: TextStyle(fontSize: 26)),
                            //if (state is GetTempAndHumState)
                            Text('${_temperature.toString()}°C',
                                style: const TextStyle(fontSize: 56)),
                            //else
                            //  const Text('Т°C', style: TextStyle(fontSize: 56)),
                            const Text('Влажность',
                                style: TextStyle(fontSize: 26)),

                            Text('${_humidity.toString()}%',
                                style: const TextStyle(fontSize: 56)),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text('Минимальная температура',
                                style: TextStyle(fontSize: 18)),
                            Text('${_currentSliderPrimaryValue.toString()}°C',
                                style: const TextStyle(fontSize: 18)),
                          ],
                        ),
                      ),
                      Slider(
                        value: _currentSliderPrimaryValue,
                        secondaryTrackValue: _currentSliderSecondaryValue,
                        min: 15.0,
                        max: 30.0,
                        divisions: 15, // Количество делений на шкале
                        label: '$_currentSliderPrimaryValue',
                        onChanged: (double value) {
                          if (value <= _currentSliderSecondaryValue) {
                            setState(() {
                              _currentSliderPrimaryValue = value;
                            });
                          }
                          widget.sendBloc.add(
                            SendMinMaxEvent(
                              _currentSliderSecondaryValue.toInt(),
                              _currentSliderPrimaryValue.toInt(),
                            ),
                          );
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text('Максимальная температура',
                                style: TextStyle(fontSize: 18)),
                            Text('${_currentSliderSecondaryValue.toString()}°C',
                                style: const TextStyle(fontSize: 18)),
                          ],
                        ),
                      ),
                      Slider(
                        value: _currentSliderSecondaryValue,
                        min: 15.0,
                        max: 30.0,
                        divisions: 15,
                        label: '$_currentSliderSecondaryValue',
                        onChanged: (double value) {
                          setState(() {
                            if (value >= _currentSliderPrimaryValue) {
                              setState(() {
                                _currentSliderSecondaryValue = value;
                              });
                            }
                          });
                          widget.sendBloc.add(
                            SendMinMaxEvent(
                              _currentSliderSecondaryValue.toInt(),
                              _currentSliderPrimaryValue.toInt(),
                            ),
                          );
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text('Статус реле',
                                style: TextStyle(fontSize: 28)),
                            BlocBuilder<RelayBloc, RelayState>(
                              builder: (context, state) {
                                if (state is GetRelayState) {
                                  _relayStatus = state.isRelayOn;
                                }
                                return Container(
                                  width:
                                      20, // Задайте ширину и высоту кружка по вашему усмотрению
                                  height: 20,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: _relayStatus
                                        ? Colors.green
                                        : Colors
                                            .red, // Измените цвет в зависимости от статуса батареи
                                  ),
                                );
                              },
                            ),
                          ],
                        ),
                      )
                    ],
                  );
                })));
  }
}
