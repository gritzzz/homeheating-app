import 'package:flutter/material.dart';
import 'package:home_heating/bloc/data/data_bloc.dart';
import 'package:home_heating/bloc/relay/relay_bloc.dart';
import 'package:home_heating/bloc/send/send_bloc.dart';
import 'package:home_heating/home_heating.dart';

void main() => runApp(const HeatingApp());
final dataBloc = DataBloc();
final relayBloc = RelayBloc();
final sendBloc=SendBloc();
class HeatingApp extends StatelessWidget {
  const HeatingApp({super.key});

  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      home: HeatingPage(dataBloc: dataBloc, relayBloc: relayBloc, sendBloc: sendBloc),
    );
  }
}







